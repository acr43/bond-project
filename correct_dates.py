import numpy as np

# correct = np.genfromtxt("cad.csv", delimiter=",", dtype=str)

# get_dates = lambda data: data[7:][:,0]
# get_fx = lambda data: data[7:][:,1]

# real_dates = get_dates(correct)

# file_name = "brl.csv"
# ouput_name = "brazil_correct.csv"
# country = np.genfromtxt(file_name, delimiter=",", dtype=str)
# country_dates = list(get_dates(country)) # will use index function so need to convert to list structure 
# country_fx = get_fx(country)

def correct_dates(country_dates, country_fx, real_dates):

	country_dict = {}
	loc = 0 # index initialized 

	for date in real_dates:

		if date in country_dates:
			loc = country_dates.index(date) # update index 

		country_dict[date] = country_fx[loc]

	export = np.array([[k, v] for k, v in country_dict.items()])

	return export

# export = correct_dates(country_dates, country_fx, real_dates)

# np.savetxt(ouput_name, export, delimiter=',', fmt="%s")


