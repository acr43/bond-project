import numpy as np
from correct_dates import correct_dates

country_name = "mexico"

def correct_bond_dates(country_name):

	country = np.genfromtxt("./bonds/" + country_name + ".csv", delimiter=",", dtype=str)[2:]
	correct = np.genfromtxt("./bonds/us.csv", delimiter=",", dtype=str)[2:]
	real_dates = [x[0] for x in correct]

	tenyr_dates = []
	tenyr_fx = []
	fiveyr_dates = []
	fiveyr_fx = []
	twoyr_dates = []
	twoyr_fx = []

	if (len(country[0]) == 11): # has px bid columns

		for row in country:
			tenyr_dates.append(row[0])
			tenyr_fx.append(row[2])
			fiveyr_dates.append(row[4])
			fiveyr_fx.append(row[6])
			twoyr_dates.append(row[8])
			twoyr_fx.append(row[10])

	else:

		for row in country:
			tenyr_dates.append(row[0])
			tenyr_fx.append(row[1])
			fiveyr_dates.append(row[3])
			fiveyr_fx.append(row[4])
			twoyr_dates.append(row[6])
			twoyr_fx.append(row[7])


	tenyr = correct_dates(tenyr_dates, tenyr_fx, real_dates)[:,1]
	fiveyr = correct_dates(fiveyr_dates, fiveyr_fx, real_dates)[:,1]
	twoyr = correct_dates(twoyr_dates, twoyr_fx, real_dates)

	export = [list(twoyr[i]) + [fiveyr[i]] + [tenyr[i]] for i in range(len(tenyr))]

	return export

export = correct_bond_dates(country_name)

np.savetxt("./correct_bonds/" + country_name + "_correct.csv", export, delimiter=',', fmt="%s")

